<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('modalidades');
});
Route::get('modalidades', 'ModalidadesController@getTodas');
Route::get('modalidades/mostrar/{slug}', 'ModalidadesController@getVer');
Route::get('modalidades/puntuar/{slug}', 'ModalidadesController@getPuntuar');
Route::get('modalidades/resetear/{slug}', 'ModalidadesController@getPuntuar');
Route::post('participantes/crear', 'ParticipantesController@getCrear');
Route::post('participantes/crear', 'ParticipantesController@postCrear');


//SOAPSERVER
Route::any('api','SoapServerController@getServer' );
Route::any('api/wsdl','SoapServerController@getWSDL' );
Route::get('rest/ganador/{slug}', 'RestWebServiceController@getVer');