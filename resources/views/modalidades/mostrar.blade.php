
@extends('layouts.master')
@section('titulo')
muestra la modalidad
@endsection
@section('contenido')

<div class="row">
	<div class="col-sm-4">
		<h1>
			{{$modalidad->nombre}}<br>
		</h1>
		<h4>
			Familia Profesional: {{$modalidad->familiaProfesional}} <br>
		</h4>
		<H1>Participantes</H1>
	</div>
</div>
<div class="row">
	@foreach ($modalidad->participantes as $participante)
	<div class="col-sm-2">
		{{$participante->nombre}}
		<img class="card-img-top" src="{{asset('assets/imagenes/participantes')}}/{{$participante->imagen}}">

		@if ($participante->puntos>-1)
		<h3>RESULTADOS</h3>
		<table bgcolor="grey" align="center" border="3px" cellpadding="3px" >
			<tr><th><strong>Nombre</strong></th><th><strong>Puntos</strong></th></tr>
			

			<tr>
				<td bgcolor="white">
					{{$participante->nombre}}{{$participante->apellidos}}
				</td>
				<td>
					{{$participante->puntos}}
				</td>
			</tr>
		</table>

		@endif
	</div>

	@endforeach
	

</div>
<a class="btn btn-warning" href="{{ url('/modalidades/puntuar') }}/{{$modalidad->slug}}" role="button"> PUNTUAR</a>
<a class="btn btn-warning" href="{{ url('/modalidades/resetear') }}/{{$modalidad->slug}}" role="button"> RESETEAR</a>



@endsection
