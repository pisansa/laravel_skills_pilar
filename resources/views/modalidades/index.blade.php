@extends('layouts.master')
@section('titulo')
Listado de Modalidades
@endsection
@section('contenido')
<h4>Todas las modalidades</h4>
@if (session('mensaje'))
<div class="alert alert-danger">
	{{session('mensaje')}}
</div>
@endif
<div class="row">
	<div class="card-deck">
		@foreach( $modalidades as $modalidad )
		<div class="col-xs-12 col-sm-6 col-md-4 ">
			<div class="card">
				<img class="card-img-top" src="{{asset('assets/imagenes/modalidades')}}/{{$modalidad->imagen}}">
				<div class="card-body">
					<a href="{{ url('/modalidades/mostrar/') }}/{{$modalidad->slug}}">
						<h5 class="card-title">{{$modalidad->nombre}}</h5>
					</a>
					
					{{$modalidad->participantes->count()}}
					
					@if ($modalidad->participantes->count()==1)
					participante
					@else
					participantes
					@endif

				</div>
			</div>
		</div>
		@endforeach
	</div>
	@endsection


