
@extends('layouts.master')
@section('titulo')
Crear Participante
@endsection
@section('contenido')
<div class="row">
	<div class="offset-md-3 col-md-6">
		<div class="card">
			<div class="card-header text-center">
				Añadir nuevo participante
			</div>
			<div class="card-body" style="padding:30px">

				<form action="{{ action('ParticipantesController@postCrear') }}" method="POST" enctype="multipart/form-data">
					
					{{ csrf_field() }}
					<div class="form-group">
						<label for="nombre">Nombre</label>
						<input type="text" name="nombre" id="nombre" class="form-control">
					</div>
					<div class="form-group">
						<label for="nombre">Apellido</label>
						<input type="text" name="nombre" id="nombre" class="form-control">
					</div>
					<div class="form-group">
						<label for="nombre">Centro</label>
						<input type="text" name="nombre" id="nombre" class="form-control">
					</div>
					<div class="form-group">
						<label for="tutot">tutor</label>
						<input type="text" name="tutor" id="tutor" class="form-control">
					</div>
					<div class="form-group">
						Modalidad<select name="modalidad">
							@foreach ($modalidad as $modalidad)
							<option value="{{$modalidad->id}}">{{$modalidad->nombre}}</option>
							@endforeach
						</select>
					</div>
					
					<div class="form-group">
						{{-- TODO: Completa el input para la imagen --}}
						<label for="imagen">Subir foto</label>
						<input type="file" name="imagen" id="imagen" class="form-control">

					</div>
					
					<div class="form-group text-center">
						<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
							Subir cuadro
						</button>
					</div>
					{{-- TODO: Cerrar formulario --}}
				</form>
			</div>
		</div>
	</div>
</div>
@endsection





<script>
        $(document).ready(function() {
            $('#tutor').autocomplete({
                source: 
                function (query, result) 
                {
                    $.ajax({
                        type: "POST",
                        url: "{{url('busquedaAjax')}}",
                        dataType: "json",
                        data: {"_token": "{{ csrf_token() }}","busqueda": query['term']},
                        success : function(data){
                           result(data);
                        }
                    });
                },
                position: {
                  my: "left+0 top+8", //Para mover la caja 8px abajo
                }
            });
      });
</script>

















