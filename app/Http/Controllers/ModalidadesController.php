<?php

namespace App\Http\Controllers;
use App\Modalidad;
use Illuminate\Http\Request;

class ModalidadesController extends Controller
{
	public function getTodas(){
		$modalidades = Modalidad::all();
		return view('modalidades.index', array('modalidades'=>$modalidades));
	}

	public function getVer($slug)
	{
		$modalidad = Modalidad::where('slug',$slug)->first();
		return view('modalidades.mostrar', array('modalidad'=>$modalidad));
	}
	public function getPuntuar($slug)
	{
		$modalidad = Modalidad::where('slug',$slug)->first();
		return view('modalidades.mostrar', array('modalidad'=>$modalidad));
	}
		public function getResetear($slug)
	{
		$modalidad = Modalidad::where('slug',$slug)->first();
		return view('modalidades.mostrar', array('modalidad'=>$modalidad));
	}
	
}
