<?php

namespace App\Http\Controllers;
use App\Modalidades;
use App\Participante;
use Illuminate\Http\Request;

class ParticipantesController extends Controller
{
	public function getCrear(){
		$participantes = Participantes::all();
		return view('participantes.crear', array('participantes'=>$participantes));
	}
	public function postCrear(Request $request){

		$participante = new Participante();
		$participante->nombre = $request->nombre ;
		$participantes->apellidos = $request->apellidos ;
		$participante->centro =$request->centro ;
		$participante->tutor =$request->tutor ;
		$participante->fechaNacimiento = $request->fechaNacimiento ;
		$participante->modalidad_id= $request->modalidad;
		$participante->imagen = $request->imagen->store('','participantes');

		$participante->save();
		return redirect('participantes/inscribir');
		
		try{
			$reserva->save();
			return redirect('')->with('mensaje','Cuadro: '. $cuadro->nombre .' guardada');	
		}catch(\Illuminate\Database\QueryException $ex){
			return redirect('')->with('mensaje','Fallo al crear Cuadro');
		}
	}

	}

