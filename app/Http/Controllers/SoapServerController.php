<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Participante;
use Auth;
use SoapServer;
use App\libPilar\WSDLDocument;

class SoapServerController extends Controller
{
	private $clase = "\\App\\Http\\Controllers\\SkillsWebService";
	private $uri = "http://localhost/laravel_skills_pilar/public/api";
	private $urlWSDL = "http://localhost/laravel_skills_pilar/public/api/wsdl";

	public function getServer()
	{
		$server = new SoapServer($this->urlWSDL);
		$server->setClass($this->clase);
		$server->handle();
		exit;

	}
	public function getWSDL(){
		$wsdl = new
		WSDLDocument($this->clase,$this->uri,$this->uri);
		$wsdl->formatOutput = true;
		header('Content-Type: text/xml');
		echo $wsdl->saveXML();
		exit;
	}
}

class SkillsWebService{

/**
 * devuelve numero participantes centro
 * @param  string $centro 
 * @return integer
 */
public function getNumeroParticipantesCentro($centro){
	return Participante::where('centro','=',$centro)->count();
}

/**
 * devuelve participantes
 * @return App\Participante[]
 */
/*public function getParticipantesTutor($tutor){

$participantes=DB::table('participantes')->orderBy('puntos','asc')->groupBy('tutor')->having('tutor','=',$tutor)->get();
}*/

}
