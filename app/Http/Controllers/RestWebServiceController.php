<?php

namespace App\Http\Controllers;
use App\Participante;
use Illuminate\Http\Request;

class RestWebServiceController extends Controller
{
 public function getVer()
	{
		$participante = Participante::max('votos');
		return response()->json($participante);
		
	}
}
